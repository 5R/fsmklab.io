---
layout: post
title: "TFSC BMSIT"
date: 2015-06-05 10:25:33
category: glugs
---

The Free Software Club (TFSC), also known as the GNU/Linux Users Group (GLUG), is a student club of technology and FOSS enthusiasts in BMSIT. As the name of the club implies, we are huge fans of Free and Open Source Software (FOSS), and one of the main objectives of this club will be to promote free software among its members, the college, and the world in general. We meet on a weekly basis to discuss new technologies, conduct sessions on various topics, or just hang out and learn something new.
