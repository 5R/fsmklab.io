---
title: STOP INTERNET CENSORSHIP - STOP BLOCKING WAY BACK MACHINE
category: press
layout: post
pinned: false
date: 2017-08-09 00:00:00 +0000

---

In a move that can only be seen as regressive, the Government of India has blocked access to the website ‘archive.org’, also called ‘Wayback Machine’. No official reason has been given for this block.

As the name itself indicates, the website is a digital archive of material available over the World Wide Web (www). It is a non-profit organisation which preserves web pages and content from being lost. It is an invaluable resource for people whose site may have crashed or no longer available for other reasons or even previous versions of the same site. With more than 450 billion pages archived, it is a vast wealth of knowledge on the Internet.

The use of archive.org has become significant in recent times to retrieve several documents which were hosted on UIDAI website and subsequently removed. Internet activists have been using archive.org to capture such documents.

FSMK strongly condemns this unilateral decision to ban and block websites,which points to Internet censorship and a lack of transparency. FSMK demands that the government revoke this ban with immediate effect and ensure that such censorship does not occur in the future.

Shijil TV

Secretary

<a href="https://cloud.fsmk.org/index.php/s/x6lsYMj4lIdfVr6">Press Release - STOP INTERNET CENSORSHIP - STOP BLOCKING WAY BACK MACHINE</a>
