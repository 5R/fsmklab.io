---
layout: post
title:  "On Pegasus Spyware and WhatsApp Surveillance"
date:   2019-11-04
category: press
---


The Free Software Movement of Karnataka strongly condemns the targeted
surveillance of activists, lawyers and journalists with the Israeli
Pegasus spyware. WhatsApp itself has accepted that over 1,400 people
were targeted globally through its platform. The targets include over
40 politicians (earlier reported to be 24), lawyers, journalists and
academics in India. Many of those targeted are involved in the Bhima
Koregaon case while others are prominent critics of the government.

The targeting of these individuals is unconstitutional and is a grave
violation of democracy and the privacy of these individuals,
especially in light of the Supreme Court’s judgement declaring privacy
as a fundamental right. As per the Telegraph Act, the Supreme Court’s
judgement in the PUCL case and the current legal framework,
surveillance of an individual can be done only on the basis of an
order signed by the Home Secretary. Even this surveillance covers only
the interception of messages and does not permit any hacking of
people’s devices. The use of spyware such as Pegasus is equivalent to
an illegal search and seizure of the data and information held
privately by an individual on his or her device. While the Cyber &
Information Security Division of the Ministry of Home Affairs has said
it has no information of the purchase of Pegasus, the government is
yet to declare that no department has purchased the spyware.  The
Indian government must clarify if any of its agencies have used
Pegasus against anyone in the country and must immediately take action
against those responsible. The theft of personal information must be
punished as per the law.


FSMK also demands the passing of a comprehensive data protection law
which safeguards the rights and information of individuals.



General Secretary, <br> 
Naveen Mudunuru
