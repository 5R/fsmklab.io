---
layout: post
title:  "On revoke the Guidelines for Patenting"
date:   2015-12-16 18:03:41 +0530
category: press
---



The Free Software Movement of India (FSMI) welcomes the decision of the Patent Office, made vide its Order of December 14, 2015, to revoke the Guidelines for Patenting of Computer Related Inventions (CRIs) issued on August 21, 2015.

The Patent Office has stated in its aforementioned order that having received several representations regarding the interpretation and scope of Section 3(k) of Patents Act, it has decided to keep the Guidelines of August 21, 2015, under abeyance till contentious issues are resolved.

As stated by the FSMI in its statement of http://fsmi.in/content/patent-office-guidelines-not-tune-patents-act, the Guidelines on Patenting of CRIs issued on August 21, 2015, violate the law as contained in the Patents Act (as amended).  We reiterate our call to immediately amend the Guidelines, to bring them in consonance with the letter and spirit of the Patents Act. Further, the FSMI requests the Patent Office to ensure that all proposed consultations on the issue are carried out in public and with full transparency as regards the proceedings. In particular, we strongly urge the Patent Office to publish (on its website), all comments and submissions received apropos this issue so as to enable public scrutiny of the rule making process.

Finally, the FSMI lauds the efforts of the programmers, entrepreneurs and interested citizens who raised their voices against the aforementioned Guidelines for Patenting of CRIs and call upon the community to continue to be vigilant against attempts to restrict the innovative capabilities of
Indian citizens
