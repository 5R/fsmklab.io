---
title: FSMK welcomes Supreme Court's judgment that declares Right to Privacy a fundamental right
category: press
layout: post
pinned: false
discourse_topic_id: 995
date: 2017-08-24 00:00:00 +0000

---

FSMK welcomes the historical judgment by a nine judge Constitutional Bench of the Supreme court that declared the right to privacy a fundamental right under Article 21 of the Indian Constitution. Article 21 seeks to protect the life and personal dignity of an individual. “This right to privacy is protected under Article 21 and it is intrinsic to the Constitution,” said Justice J Chelameswar, reading out the verdict for the nine-judge bench.

The Supreme Court has also overruled its own eight-judge Bench and six-judge Bench judgments of M.P. Sharma v. Satish Chandra, and Kharak Singh v. State of Uttar Pradesh respectively which had held that privacy is not a fundamental right guaranteed protected under the Constitution.

The current judgment, while emphasizing on the many facets of privacy, including personal intimacies, sexual orientation, and informational privacy, held privacy to be an essential ingredient for an individual to exercise the many freedoms guaranteed under the Constitution. Justice Chandrachud in his opinion acknowledged that with the immense technological advancements, the personal information of an individual is being electronically tracked on the internet, and at times used to create profiles. With this judgment, broad contours of the right to privacy have been mentioned.

The judgment will have a crucial bearing on the government’s Aadhaar scheme which collects personal details, biometrics etc and poses a threat of forming a surveillance state. However, the court today has not ruled on the reach of Aadhaar - that will be decided separately by a smaller bench. Hence the fight is still on.

In the context of new judgment, we urge the government to immediately pass the privacy and data protection bill that respects the privacy of all citizens of our country. On this triumphant day, FSMK would like to congratulate all the lawyers, petitioners, activists and others who fought for the #RightToPrivacy.

Shijil TV

Secretary

<a href="https://cloud.fsmk.org/index.php/s/nbUpbvXkfFGlkPX">Press Release - FSMK welcomes Supreme Court's judgment that declares Right to Privacy a fundamental right</a>
