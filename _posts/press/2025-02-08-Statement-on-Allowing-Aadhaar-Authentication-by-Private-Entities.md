---
layout: post
title:  "Statement on Allowing Aadhaar Authentication by Private Entities"
date:   2025-02-08
category: press
---

<br>
<p>The recent move by the Government of India to expand Aadhaar authentication
for businesses raises serious concerns about privacy. By allowing private entities
to use Aadhaar for verification, the government is risking the misuse of sensitive
biometric and demographic data, exposing millions to potential breaches and
surveillance. This decision undermines the fundamental right to privacy, as
affirmed by the Supreme Court in the Justice Puttaswamy case, and opens the
door to unchecked data exploitation by corporations.</p>
<p>Moreover, the over-reliance on Aadhaar for service delivery continues to exclude
marginalized communities. The MGNREGA scheme is a stark example, where
authentication failures and technical glitches have denied wages to countless
workers, leaving them without a safety net. Expanding Aadhaar's use in the
private sector will only exacerbate such exclusions, as those without Aadhaar or
with authentication issues will be barred from essential services.</p>
<p>The government’s push to legalize Section 57 of the Aadhaar Act, which was
struck down by the Supreme Court in 2018 for enabling private entities to
mandate Aadhaar, is deeply troubling. This move disregards the Court’s ruling
and risks reviving the same privacy violations the judgment sought to prevent.
Additionally, the government has failed to disclose the public consultations
received for the Aadhaar Act 2020 amendments, raising questions about
transparency and accountability. Why is the government withholding this critical
information from citizens?</p>
<p>Technology in the public realm should empower citizens, not render them
vulnerable, powerless or exclude.</p>
<p>FSMK strongly opposes & condemns the amended rules to the Aadhaar Act and
calls upon the people of Karnataka to also voice out their opposition strongly.
The welfare of citizens must not be sacrificed at the altar of technological
overreach.</p>
Naveen Mudunuru<br>
Secretary, FSMK

<object data="/files/PressStatement.pdf" width="1000" height="1000" type='application/pdf'></object>
