---
layout: post
title:  "On blocking of GitHub and other sites"
date:   2014-12-31 21:06:01 +0530
category: press
---







Over the last few days, there have been reports that internet users in India are not able to access services like GitHub, PasteBin and Imgur. It is now increasingly becoming clear that these sites have been blocked by the ISPs (Internet Service Providers), with no explanation given. One ISP claims that these sites have been blocked as per the instructions of Competent Authority.
No mention of who the Competent Authority is nor the rationale behind the blockade have been provided. The situation with other ISPs is even worse, with a simple message that states “Service Unavailable”. Netizens on the social media from across the country have been reporting on this issue, which confirms the fact that this is not a single ISP acting in isolation but a concerted effort at blocking these services. This could not have happened with out the notice of the Government.
It is shocking to note that a popular code-hosting website like GitHub, which is very popular among researchers, scientists, developers and students alike, and PasteBin are blocked. This brazen act of censorship goes against the fundamental rights of the citizens and the absolute lack of transparency in these matters is an attack on democracy and scientific progress.
FSMI and FSMK wants that the Government of India to immediately revoke this unjust blocking of these services, issue guidelines and ensure transparency in the process.
