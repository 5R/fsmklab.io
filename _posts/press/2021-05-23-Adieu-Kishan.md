---
layout: post
title:  "Adieu Kishan Gupta"
date:   2021-05-23
category: press
---
{:refdef: style="text-align: center;"}
![Kishan Gupta](/images/kishan_gupta_5_23_2021.jpg)
{: refdef}

<br>
The entire Free Software Movement Karnataka (FSMK) family is in shock at the untimely demise of Kishan Gupta. Kishan was the prodigious kid who came to the 2014 camp as a participant and turned out to be a resourceful technical rockstar. His sense of humour  was unique. He was a well-read and articulate free software activist who took well-reasoned positions on politics and technology. His clarity on any topic was commendable. His contribution to all discussion were always insightful. One such discussion thread Kishan initiated is still relevant for all free software enthusiasts. [Link to thread](https://discuss.fsmk.org/t/what-is-the-purpose-of-a-glug/)

Kishan was an FSMK executive committee member and an inspiring free software activist. He has played various roles during his association with FSMK, as a glug member, as a resource person in workshops, as a system admin for FSMK, as a friend and more.

He was a hacker in all aspects. Kishan started his free software journey with FSMK while he was studying in BMSIT college. He was an active contributor in BMSIT free software group. He was in charge of FSMK's server infrastructure. Kishan was a go to person for all solutions to technical problems. He gave us the confidence to  broadcast Camp 14 sessions to multiple class rooms through Big Blue Button, a new experience for us, during a live camp. Hundreds of students familiarised using GNU/Linux using the distros set up by Kishan on multiple occasions.


We can write pages after pages about Kishan and our memories of him. We at Free Software Movement Karnataka are devastated by this loss. We miss you Kishan.