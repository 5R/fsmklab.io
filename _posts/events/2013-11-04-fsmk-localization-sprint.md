---
layout: post
title: "FSMK Localization Sprint"
date: 2013-11-04 06:18:38
category: events
---

On the context of Kannada Rajyotsava, FSMK's Localization Chapter has decided to have a month long sprint to localize various Free Software tool/applications. The aim of this sprint is to create awareness amongst its members about the need for localization and how anyone, even without any technical knowledge can contribute very easily to free software community by localization. Many of the FSMK members have already started contributing actively to GNU Health project and we have decided to have a focussed effort to make significant progress in localizing the project. Thanks to the concentrated efforts put by some of the FSMK members, in last couple of weeks.

Tthe Kannada localization of GNU Health has already reached 15%. We would encourage all the members to join this localization sprint and help us take Kannada localization of GNU Health to 100%.

For many of our members who are not comfortable with Kannada, please use this opportunity to contribute to the project in any language that you are comfortable with.  
New to localization, check it out below link...

Demo on GNU Health Localization

To type in Kannada

For Linux Machines  
You can use offline tools like ibus   
Read the document of installation in the below link  
https://help.ubuntu.com/community/ibus  
For, Kannada language in particular you can look at the below link for installation.   
http://askubuntu.com/questions/269514/how-to-type-in-kannada-language-in-libreoffice

For Windows Machines  
You can use online translation tool like    
1)http://translate.google.com/  
2) http://www.quillpad.in/index.html  
3) You can also  install  Google IME  software for Offline translation.   
http://www.google.com/inputtools/windows/index.html

In case, you are looking for a specific word to translate in Kannada, you can use the following websites:  
1) https://kn.wiktionary.org/  
2) http://baraha.com/kannada/index.php

See the below Facebook Page for more information on the Localization Sprint FB Page

 
