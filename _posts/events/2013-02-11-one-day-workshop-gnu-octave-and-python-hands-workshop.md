---
layout: post
title: "One Day Workshop on GNU Octave and Python Hands on Workshop"
date: 2013-02-11 17:53:52
category: events
---

Numerical computation tools that run on GNU/Linux platform such as Ubuntu, Debian or Fedora are a huge blessing for all mathematical computation ‘freaks', be it students or researchers.  
The most famous Free Software tools are GNU/Octave, Scilab, and the powerful Python language (with its gamut of libraries) — all licensed under the GNU General Public Licence.

At FSMK, Free Software Activists organise and conduct a workshop on basics of Digital Image Processing using Free Software utilities like GNU Octave and Python programming .

Schedule:

Why Signal processing, and why on Free Software platforms by Raghavendra S, BMS Institute of Technology.  
Basic Image processing using Fourier and Wavelet transforms by Chetan Murthy, BMSIT  
Image Processing and beyond using Python Programming by Sneha Das, BMSIT  
Registration Fee: Rs.100/-

Date: 6th April, 2012  
Time: 9:30 AM  
Venue: FSMK Office, (Opp. Old Harilakshmi Theatre)  
No. 121/17, 1st Floor, 6th main,  
14th Cross, Wilson garden, Bengaluru - 560030.

 
