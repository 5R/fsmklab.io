---
layout: post
title: "Industry Orientation Workshop on Free Software Technologies 2013 @JVIT"
date: 2015-03-06 18:29:44
category: events
---

FSMK camps are technologically intensive and the effort is always to ensure that during the span of the camp students are exposed to as many different free software technologies and free software tools as possible. Few of the topics which are popular and are considered to be hot technologies are given more emphasis such that students can become atleast novice developers using those technologies. For the Summer Camp, following technologies will be emphasized on.

1.  Converting to GNU/Linux User
2.  Developing Standalone Applications using Python/Qt
3.  Developing Web Portals using Drupal
4.  Developing mobile Applications for Android
