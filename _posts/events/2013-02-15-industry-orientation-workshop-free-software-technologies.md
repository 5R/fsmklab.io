---
layout: post
title: "Industry Orientation Workshop on Free Software Technologies"
date: 2013-02-15 03:55:49
category: events
---

Date: 26th – 30th Jan 2013 Venue: Sai Vidya Institute of Technology, Doddaballapur Organized by: Free Software Movement Karnataka Register: Click Here Facebook Event: Click Here

Free Software movement Karnataka (FSMK) is organizing 5 days Industry Orientation workshop on preparing participants on Free Software technologies for the software industry. Currently software industry is actively adopting free software to reduce the cost and have much more freedom to modify the software to suit their needs. This has resulted in industry's growing need of software professionals with expertise in free software technologies. This workshop is to ensure that participants are trained on various free software technologies so that they are better prepared for the industry.  
The workshop will be a hands on workshop where students will gain knowledge on various technologies like Java, Python, Networking in Linux, Web Programming. The sessions will be conducted by industry professionals with vast amount of work experience. At the end of the workshop, participants will be equipped with knowledge of free software in various domains so that they can choose whichever domain they are interested in and start working on it right during their college days.

Technical Syllabus The Technical part of the workshop will have three main parts.

Free Software Technologies (6 hr/day): This part will deal with sessions on various free software technologies. The idea of this part is to give enough information to students about various technologies so that they can then choose their domain of interest and continue learning it.

Friendship with GNU/Linux system  
Introduction to GNU/Linux shell commands  
Understanding GNU/Linux file systems  
Understanding GNU/Linux processes  
Becoming familiar with vi/vim editor  
Networking in UNIX  
Introduction to basic IP Address/gateways/routing  
Configuring services like Apache2/Dynamic-DNS/ssh  
Understanding networking commands like telnet, ssh, ping, route,ifconfig  
Java  
Introduction to OOPS concept using Java  
Brief introduction to developing android apps using Java  
Python  
Introduction to python  
Developing desktop applications using Py/GTK  
Web Programming in LAMP  
Introduction to LAMP stack  
Developing web applications using PHP

Free Software Contributions(1 hr/day): This part will deal with sessions on how to contribute to free software. It will be a one hour per day session touching on various aspects of free software contribution.  
Free Software Introduction  
History, significance  
Licenses  
Migration  
Interaction with free software community  
IRC  
Mailing list  
Etiquette  
Bug triaging  
Reporting bugs  
Bug triaging  
Where is the source code  
Concept of Code revisions  
Tools like git  
Free online project hosting like github  
Localization  
Need for localization  
.po files  
Tools for localization

Free Software in Industry(1 hr/day): This part will have sessions about the increasing trend of usage of free software in the industry. Various industry personnel with vast amount of experience will be joining us to discuss on how they have adopted free software in their industry. It will be a one hour session per day. Few listed topics are as below:  
Entrepreneurship using Free Software  
Free Software in Quality Testing industry  
Free Software in System Administration  
Free Software for Small/Medium Enterprises  
Concept of freedom in Hardware  
Free Software in Enterprise Server Market  
Free Software in Cloud Computing
