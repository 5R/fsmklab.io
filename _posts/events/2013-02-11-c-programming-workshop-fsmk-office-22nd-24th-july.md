---
layout: post
title: "C Programming Workshop @ FSMK office from 22nd to 24th July"
date: 2013-02-11 17:32:02
category: events
---

FSMK will be organizing a 3 day programming course for students in 2nd or 3rd year in Engineering or BCA at FSMK office from 22nd to 24th July. This course is based on C Programming language. The following topics will be covered in detail during the three day course:

**Part 1:**

Introduction to programming With GCC and GDB  
Best Programming practices- Naming conventions, comments, creating libraries and header files  
GCC - GNU Compiler  
GDB - GNU Debugger  
Profiling a C Program using GProf to help optimize the code

**Part 2:**

Data Structures with pointers and memory management  
Multi-dimensional arrays  
Structures and Unions  
Memory management and Pointers - malloc, calloc, free, Strings  
Linked Lists and their applications  
Stack, Recursion and Queue and their applications

**Part 3:**

Problem solving strategies  
Examples on using basic algorithms.  
Identifying best algorithms for a given problem

The course will terminate with a online programming contest in which students will be asked to solve programming problems in C in a given time and the top teams will be given appropriate prizes. Also course completion certificate will be issued on the last of the course.

The fees for the course is Rs. 500 per student and a maximum of 25 students will be entertained with first come first serve basis. Registration fee for FSMK Glug members will be Rs.300/-  
Students should bring their own laptop as the course will be mostly hands on. Students should also have any GNU/Linux based distribution installed on the laptop and should have gcc, gdb and gprof packages installed. Please send an email to anyone below if you are interested to participate in the course along with your Name, College, Branch and Year.

Date: 22nd to 24th July 2011.  
Time: 10:00AM to 5:30PM  
Venue: FSMK Office, Wilson Garden, Bangalore.

T. Vignesh Prabhu  - stove311987@gmail.com (9535321976)  
Karthik S          - iamsjkk@gmail.com (9620207665)  
Prabodh CP         - prabodhcp@gmail.com ( 9844205442)  
Raghuram Narayanan - informtoraghu@gmail.com (9591042333)

 
