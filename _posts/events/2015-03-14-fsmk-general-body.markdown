---
layout: post
title:  "FSMK General Body"
date:   2015-03-14 11:10 +0530
category: organisation
category: events
---

Free Software Movement of Karnataka is conducting its general body meeting on 7th May 2011(Saturday) at FSMK office.

Address

FSMK Office
No: 121/17, Near Old Hari Lekshmi Theater
6th Main Road, 14th Cross,
Wilson Garden, Bangalore- 560030

Former vice-chancellor Prof. Chidananda Gowda will be inaugurating the meeting.
Convener of the organization will be presenting the report of last one year activities.
The general body will be electing new office bearers of the Organization.
Also it will be discussing on the immediate and long term agendas to be taken up by Free Software Movement of Karnataka.
