---
title:  FSMI's 2nd National Conference - 4CCon
category: events
layout: post
date: 2016-12-16 00:09:00 +0530
discourse_topic_id: 482
image: fsmi_2nd_national_conference.jpg
---

Free Software Movement of India (FSMI) welcomes you to [4CCon] (Collaboration, Contribution, Communities & Commons). India's biggest gathering of Free Software activists, geeks, hackers and socially conscious tech enthusiasts.

At 4CCon, you will meet India's smartest upcoming programmers and veteran technologists, students and academics, who are in the forefront of innovation and expansion of technology. During the conference, delegates and participants will discuss a variety of issues ranging from Digital India, Smart Cities, Digital Commons, Gender and Technology and establish collaborative networks that will span the country in the coming years.

The conference falls on 27th & 28th of January 2017. Prior to conference there is also a Technical Workshop on 26 Jan 2017 with topics ranging from Go, Python, Machine Learning, Artificial Intelligence, 2D/3D Graphic Designing, IoT, GIS Mapping, Frontend Javascript frameworks, etc.,

Please note that this registration is only for the 2 day conference that will be held on 27 and 28th of January 2017. Registration and tickets for Workshop has to be obtained separately.

For more detail check <https://4ccon.fsmi.in/register>

[4CCon]: https://4ccon.fsmi.in
