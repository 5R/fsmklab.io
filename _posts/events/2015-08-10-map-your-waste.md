---
layout: post
title: "Map Your Waste"
date: 2015-08-10 14:49:05
category: events
discourse_topic_id: 30
---

On 14th August, Free Software Movement Karnataka is organising an open street map(OSM) event . The idea is to use your smart phones with this OSM software to map the sewage lines and public toilets online so that we can quickly identify clogs and ensure that the govt takes action accordingly.

You could also be part of this activity just by following these instructions.

Step 1. Do the survey: Install the OSMTracker app in phones if using Android smartphones GPS, else take fieldpapers(http://fieldpapers.org/) printout of the area if using fieldpapers for survey.  
Step 2: Upload the data to user's OSM profile from OSMTracker. Use the same to add the data to OSM database.  
Step 3: To add points, use manhole=sewer tag and to add sewage lines, use manmade=pipeline with type=sewer tags.

Go out, after your college ends on Friday and do the survey in the vicinity. Get your friends with you; after all, more the number of people, the faster we can map out the sewage network.

Download OSM apps here

<div dir="auto">
  <a href="https://f-droid.org/repository/browse/?fdid=net.osmand.plus" target="_blank">https://f-droid.org/repository/browse/?fdid=net.osmand.plus</a>
</div>

<div>
  <div dir="auto">
    <a href="https://f-droid.org/repository/browse/?fdid=net.osmand.srtmPlugin.paid" target="_blank">https://f-droid.org/repository/browse/?fdid=net.osmand.srtmPlugin.paid</a>
  </div>
</div>

<div>
  <div dir="auto">
    <a href="https://f-droid.org/repository/browse/?fdid=net.anzix.osm.upload" target="_blank">https://f-droid.org/repository/browse/?fdid=net.anzix.osm.upload</a>
  </div>
</div>

<div dir="auto">
  <a href="https://f-droid.org/repository/browse/?fdid=me.guillaumin.android.osmtracker" target="_blank">https://f-droid.org/repository/browse/?fdid=me.guillaumin.android.osmtracker</a>
</div>

This is just a begenning :D

For more detail contact Aditya Saky, +91 9538675500
