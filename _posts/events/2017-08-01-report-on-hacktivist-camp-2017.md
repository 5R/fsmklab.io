---
title: Report on Hacktivist Camp 2017
category: events
layout: post
pinned: false
date: 2017-08-01 00:00:00 +0000
startdate:  2017-08-28 00:00:00 +0000
enddate: 2017-08-30 30:00:00 +0000
discourse_topic_id: 962
---

Day 1 - 28/07/2017
Started around 11:30 am
Self-introduction of the participants and the speaker.
Discussion on GNU/LINUX and its history ( evolution of free software and about RMS, Linus Torvalds and Eben)
Discussion on the licenses and Creative Commons.
Lunch Break at 2:15 pm

Second half session started around 3:30 pm.
Started with a game organized by Ramaseshan, that focussed on Policy Design in the health segment. The game also tried to simulate the real world scenrarios and was focussed on a few aspects of the society.

Defining what "people" and "society" means

Optimal decision making

Re-defining who is a leader.

Once the game was finished, there was a discussion regarding the game and its aspects

The game goes like this.
Each participant was given a role such as Manual scavenger, homemaker, doctor, IT employee and the politician,etc. And was asked to stand on their respective levels based on their current economical condition in the country. Manual scavenger were at the bottom most level and the politician at the top 3rd level. Many chits which contains few actions which in turn after discussing with the entire team becomes the policy at that round was given and asked to select any one of them by giving a time limit. The actions were such as Hiring a doctor, basic health and education center, growing trees,etc.
At each round, the participants were asked to throw a ball on the goals that is of three concentric circles the outer most contains 20 points, the middle one 50 points and the first one 70 points.
The top level people without much efforts used to gain more points, whereas the bottom level people have to work so hard to gain points. But, still when the policies are made it directly or indirectly affects the Bottom level people the most. After each round, based on the effects that is made on the each sector of the people by bringing up that policy is either makes the particular sector of people to go one step forward or one step or level backward.
Ended up by 5:20pm.

Break

Started again around 6:40 pm at the nearby the pond (Campus has couple of such ponds)
Session on introduction to Git and few commands to use it.
Assignment was given to the participants on Git. They were asked to work on it together after the dinner. The entire strength was divided into different teams.
Finished around 8:00 pm.

Again continued with the session around 8:20 pm at one of the rooms in the guest house where our accommodations were arranged.

Dinner 9:00pm

*10:15 pm started working as a team on the assignment. With
3 teams working one after the other.

Winded up first day's program around 11:30 pm.
********** End of Day 1********

Day 2(i.e.,29/07/2017)
Went on the streets to track using OSM around 6 am. Divided into different teams one to track the buildings or the important places, the other to map the roads and another team to use the Mapillary application and took pics of the places. Through this activity, we were able to plot upto 310 nodes on OSM. The points are yet to be uploaded after cleanup and re-verification.
After breakfast the session started around 10 am.
Ramseshan handled for a while by giving some problems and asked the participants to start coding on these problems and also discussed on the solutions.
Finished around 10:45 am.
Balaji Kutty handled the session on the very basic or fundamental technologies which are essential for the IT industry in the current situation.
Break for 5 minutes.

The topic was "On the Shoulders of Giant" people spoke about the different revolutionists the science world
About Galileo Galilei by Balaji
About Dr.B.R.Ambedkar and also about the similarities between Ambedkar and Ramanujacharya by Senthil.
About Dr.Sunitha Krishnan "The fearless women" by Shashank. A video was screened regarding her.
About Movement (definition and meaning) by Rameez.

A video was screened on how a movement is built. As part of it, held discussion on how FSMK as a movement has evolved by recapping the past 8 years major activities of FSMK( like Campaigns, Camps, Protests, activities in Glug, Community Centers and Freedom Hardware "Embedded for her " activity which is currently going on, etc)
Lunch break

Second half session started around 3 pm by Balaji and Vanilla Balaji.
It was rather than a usual session was a dialogue based approach to explaining licenses that are there in FOSS(such as Creative Commons, GPL, etc)
Then the screened Vanilla Balaji's works using GIMP.
Finished around 3:30 pm.
Fun activities till 3:55 pm

Senthil started the session on Current Condition of IT industries and it's Employees.
Completed by 4:45 pm.
Discussions on it up to 5:00 pm.
Tea Break till 5:15 pm.

Back to the guest house or the living area.
Later everyone went to the nearby famous historical place "Rayagopura, had fun. Sung songs, Shouted Free Software slogans and one person danced.
Dinner

Around 9 pm a general Meetup was organized. Everyone was divided into teams and was asked to write down the targets they have for the organization development. It was like the next 6months goal. Were asked to write down the targets, why choosing that Target, how are we going to accomplish it and the assumptions made while writing it.
This will be presented by each team tomorrow by choosing one common Target by discussing with the team members in each team.
************End of Day 2***********

Day 3(i.e., 30/07/2017)
Trekking to the nearby hill.
photo_2017-08-01_14-38-24.jpg1280x959 160 KB
Started the session on UID / around 9:50 am by Ganesh.
It was about the UID Aadhar which is made mandatory.
Screened two videos related to it.
Discussions went on.
Finished around 11:50 am.

Discussion on Freedom Hardware by Niranjan.

Informed about the "Embedded for her" event which is currently going on.
Finished around 12:20 pm.

Review of FSMK's activities during the last 6 months by Shijil and he ensured that the documeregarding it will also be shared in FSMK's discuss group.

Break

Presenting all the targets which were figured out yesterday by each team.
The teams were
Community Center team
Hassan region team
Mandya region team and
Bangalore region team.
End of the sessions and the Hacktivism camp....

Many people went to the historical place named DanushKoti and moved back to their hometowns.
More than a Camp or Technical talks or sessions, it was a movement building activity.
