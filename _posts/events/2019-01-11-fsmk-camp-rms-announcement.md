---
title: "A surprise at FSMK Camp happening in PESCE, Mandya"
category: events
layout: post
date: 2019-01-11 00:00:00 +0000
---

To those of you who dont know Dr Stallman (aka RMS), he launched the [Free software](https://www.gnu.org/philosophy/free-sw.html) movement in 1983. 
FSMK organises camps every year focused at enabling students on various free software tools and technologies which helps them add career value. 
But why are we suddenly talking about both these ? If you guessed it right, yeah, RMS is coming to FSMK Camp this year. Yay ! 

This year, [FSMK camp](https://camp.fsmk.org) is happening from 20th to 24th January 2019, at PES college of Engineering,Mandya. RMS will be giving a talk on "A Free Digital Society" from 2PM - 5PM on 20th January 2019 (ie, first day of the camp). RMS's talks are not technical in nature and even if you are not a "techy" you will be able to understand his talk. The talk is open for public (entry is free of cost) and we welcome all to come in and attend. You can register for the talk [here](https://rms-tour.gnu.org.in/registration/) (select Mandya,FSMK Bootcamp 2019) if you wish to or just drop by. We will be very happy to see you there.

![RMS_FSMK_POSTER_CAMP19](/images/fsmk_camp_19_rms_poster_final.png)
We at FSMK believe this is a great opportunity for all our camp participants, volunteers and everyone else attending the talk to interact with and hear the perspective of RMS. 

If you wish to register for FSMK camp as a participant, [click here](https://camp.fsmk.org) and register. We promise you will have one hell of an experience. 

Bidding adieu for now,

FSMK