---
layout: post
title: "Sunday School  Learning Git (Basics + Advanced)"
date: 2015-03-13 20:18:20
category: events
---

For participants of FSMK Summer Camp 2014 and anybody else interested in learning Git. Basics: What's and Why's of Git and a few basic commands to get started. Advanced: Advanced commands with a practical exercise of collective collaboration over a network to highlight the advanced features of Git. Requirements: * Get a laptop with any GNU/LINUX based OS installed. If possible get git also installed. Use the command: sudo apt-get install git If you don't have a laptop, you can still attend, we will try to provide a machine or you may have to share with someone. Sunday, August 3, 2014at 11:00am - 1:00pm @FSMK Office,No. 121/17, 1st Floor, 6th main, 14th Cross, Wilson garden, Bengaluru - 560030. Ph: 080-42817353
