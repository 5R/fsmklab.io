---
layout: post
title: "FSMK Sunday School : AngularJS and Algorithms"
date: 2015-03-13 20:17:15
category: events
---

Building Web Apps using AngularJS and  
Algorithms group's first meet-up: Divide and Conquer Algorithms-1  

By [Karthic Rao][1] and [Abhiram Ravikumar][2]

 [1]: https://www.facebook.com/karthick.rao1
 [2]: https://www.facebook.com/abhi12ravi

Sunday, August 10, 2014at 10:30am - 1:00pm

@FSMK Office,No. 121/17, 1st Floor, 6th main, 14th Cross, Wilson garden, Bengaluru - 560030. Ph: 080-42817353

 
