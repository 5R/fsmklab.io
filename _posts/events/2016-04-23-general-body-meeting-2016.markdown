---
layout: post
title:  General Body Meeting 2016
date:   2016-04-23 07:51 +0530
category: events
discourse_topic_id: 242
---


Hello!Greetings to all,

After thorough planning and rescheduling, FSMK has decided to hold itsAnnual General Body meeting on 24th April 2016, at FSMK Office, WilsonGarden,Bangalore. As part of this, we have come up with following procedureto get members from various sections of society to FSMK GB. General Body isthe core group of FSMK which takes majority of the decision for theimmediate direction of the movement. However our FSMK Discuss mailing listexists as an open group for much larger and wider audience to leaddiscussions for the movement in general. Importance of this General BodyMeeting is that more FSMK activists will meet and engage in a detaileddiscussion on the various activities pursued by FSMK and its community,review of those activities, and finalizing action plan.

Mainly, we have 4 sections of society that we have identified: Studentsfrom our GLUGs, Academicians, Community Center members and Workingprofessionals(non-academicians). From our understanding, this should covermore or less everybody associated with FSMK. For each of them, we have comeup with a procedure to become part of FSMK General Body.

For GLUG members:GLUGs are requested to nominate delegates to the General Body. We suggest the following process to make this effective:

1. How can GLUGs elect its delegates?Call a meeting of the GLUG members ie whoever are associated with freesoftware activities. Brief them about the FSMK General Body meeting and itsimportance. Democratically decide who could be delegates to FSMK GB.

2. How many delegates a GLUG can elect?For every 10 GLUG members, 1 delegate may be elected by the studentsamongst who are gathered. If calling all the GLUG members for the meetingis very difficult at this point, atleast you call a meeting of the coreGLUG members and finalize the names of the delegates to be sent to Generalbody meeting. For example, if you have 50 GLUG members, you may elect/finalize 5 delegates who can attend the FSMK GB meeting.

3. Where will the FSMK GB meeting happen? At FSMK Office.

4. What is the role of GB member?A delegate who gets elected by the GLUG and attends FSMK GB will beincluded in the FSMK-Core mailing list. He/she would participate and workfor building the free software activities not just in GLUG but at anorganization level at FSMK. Such delegate will also have an opportunity toget elected further into the higher committees which will be decided in theFSMK GB meeting. He/she will also be responsible in ensuring that whatevergets discussed/decided in FSMK core is also passed on to the GLUGregularly.

For Community Center members:Since only 2 community centers are currently active, we have suggested themto nominate 5 members together from the same.

For Working professionals: Since there is currently no unit as such to represent working professionalsin FSMK currently, we have decided that any working professional interestedto become part of General Body will have to pay Rs. 1000. Though this doessound as someone buying his/her way into FSMK General Body, we currently donot have any other better way for deciding who should be and who shouldn'tbe part of FSMK General Body from working professionals. We believe thatthe fee becomes a self-detterant to decide whether a person wants to commithimself/herself to become a General Body member and it will ensure thatonly those who see long term commitment to the movement will come forward to pay the fees. We are open to any suggestions for rectifying this process.

For Academicians: As we do not yet have an active academicians chapter, we have decided toask academicians also who want to become part of FSMK also to pay Rs. 10 tobecome part of General Body.

Please note that we consider the above plan as a guiding document for us togo ahead and organize a general body which will help us to re-groupourselves with more active members, come up with a more vibrant set ofactivists to lead the movement.

When (is the GB)? On Sunday,24th April, 2016 from 10:30 to 1:00 IST

Where? FSMK Office
No: 121/17, Near Old Hari Lekshmi Theater6th Main Road, 14th Cross,Wilson Garden, Bangalore - 560030

Thanks and Regards

On behalf of FSMK
