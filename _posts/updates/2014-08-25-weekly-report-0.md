---
layout: post
title: "Weekly Report"
date: 2014-08-25 18:48:58
category: updates
---

<div>
  News <br />*FSMK is associating with Pycon India conducting python sessions across Karnatka part of Pymonth:-<br />PyCon India, the premier conference in India on using and developing the Python programming language is conducted annually by the Python developer community, represented by Python Software Society of India. The conference attracts the best Python programmers across the country and abroad.<br />This year, PyCon India is celebrating the month of August as Python Month, with aim of increasing the awareness and awesomeness of Python Programming Language within the student community in India...<br /><a href="http://in.pycon.org/2013/python-month">http://in.pycon.org/2013/python-month</a>
</div>

 

<div>
  <div>
    <p>
      *Direct download links for FSMK Lab manuals available on FSMK site now(currently printable forms of lab manuals are ready only for odd semesters, however you can access even semester manuals here <a href="https://github.com/fsmk/CS-VTU-Lab-Manual">https://github.com/fsmk/CS-VTU-Lab-Manual</a>.<br />Know more about Docathon <a href="http://fsmk.org/?q=vtu-lab-manual">http://fsmk.org/?q=vtu-lab-manual</a> and contribute to the fsmks' lab manual project.
    </p>

    <p>
      Upcoming Activities
    </p>

    <p>
      GLUGDSC- 30th Aug -  31st Aug
    </p>

    <p>
      Hands on Session on 30th Aug -  31st Aug<br />Rakesh P Gowada will handle it
    </p>

    <p>
      TFSC Glug (BMSIT) - 30th Aug<br />Workshop on Python Programming<br />Karthik Naik will coordinate
    </p>

    <p>
      26th August 2014
    </p>

    <p>
      The second season of the above mentioned course will start on #fsmk<br />Freenode on IRC at 10:00 PM on 26th August 2014 (Tuesday). Do join in if<br />you are interested.<br />Previous sessions logs: <a href="https://www.facebook.com/groups/fsmkpythonglug/">https://www.facebook.com/groups/fsmkpythonglug/</a> or<br /><a href="http://pastebin.com/u/abhi12ravi">http://pastebin.com/u/abhi12ravi</a><br />Do watch the 6th video of the lecture series before you join the class.<br /><a href="http://youtu.be/D5C9bLnzDdI?list=PLE621E25B3BF8B9D1">http://youtu.be/D5C9bLnzDdI?list=PLE621E25B3BF8B9D1</a>
    </p>

    <p>
      Abhiram RaviKumar will handle
    </p>

    <p>
      What happened ?
    </p>

    <p>
      #SUNDAYSCHOOL<br />Part of Sunday school conducted session on Email Encryption using Thunderbird and Enigmail - 24th Aug<br /><a href="http://fsmk.org/?q=sunday-school-email-encryption">http://fsmk.org/?q=sunday-school-email-encryption</a><br />Vignesh Prabhu handled the session
    </p>

    <p>
      #FOCUSvce<br />Introduction to Free Software On 9th Aug mainly for second years as they are new to free software.<br />Vignesh Pravu and Karthik Rao coordinated the event.
    </p>

    <p>
      Installation Fest on 15th Aug<br />Installed Ubuntu 14.04 LTS, had some issues, solved it. went great, also introduced some basic commands like file operations and installing some packages, tasks given to install some useful packages like git and brackets and vim<br />Bhargav R. Panth and  Nishchal Gautam coordinated the event
    </p>

    <p>
      Hands on Session on 16th Aug <br />Covered HTML: Basic tags, heading tags, paragraphs and some tricks , Session was short but everyone was trying something we've got some students querying about different tags which we couldn't cover in just a hour of class.<br />Bhargav R. Panth and  Nishchal Gautam coordinated the event
    </p>

    <p>
      Hands on Session on 23rd Aug<br />Introduction to CSS - Interested candidates, everyone trying out different things themselves, some of them completed the assigned task which was given last week, today assigned the task to do some CSS and develop something<br />Bhargav R. Panth and  Nishchal Gautam coordinated the event
    </p>

    <p>
      #GLUGDSCE<br />Hands on Session  on HTML5 and CSS3 mainly foccus in css3 21st Aug<br />Aayush and Apoorva coordinated the event
    </p>
  </div>

  <div>
     
  </div>
</div>
