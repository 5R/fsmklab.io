---
layout: post
title: "FSMK Newsletter Volume 3"
date: 2013-04-08 18:19:57
category: updates
---

Dear Friend,


FSMK releasing its third consecutive news letter.  
We are proud to say this because of the responses and encouragement you gave us.  
This time also there are enough and more reasons to read the complete news letter in single attempt.

We are raising some serious issues in the Indian context through the Editorial.

Dr. Sabyasachi Chatterjee is talking about patent issues.

Ranjan an FSMK activist is talking about the free-software Philosophy.

Mayank, Rahul and Praveen have come up with some Technical articles.

Many items on News section from the free-software world.

Still we are expecting the critical comments from you to make this venture a success.

<a href="http://fsmk.org//sites/default/files/NewsLetter/FSMK_Issue3_August_2009.pdf" style="margin: 0px; padding: 0px; text-decoration: initial; color: rgb(38, 140, 108); font-family: Arial, sans-serif, Helvetica; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);" target="_blank">FSMK News Letter Issue 03</a>


Regards,  
FSMK EB Team.

 
