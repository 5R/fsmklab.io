---
layout: post
title:  "FSMK's stand on usage of proprietary social networks"
date:   2016-01-28 18:06 +0530
category: updates
---

About Free Software Movement Karnataka (FSMK)

FSMK, as an organization has evolved into a vibrant community of people from various sections of society bound by the common goal of a free society built with the power of freedom software-hardware-spectrum, in general, free technology. Today it has professional software developers, engineering students, professors, teachers, researchers, higher secondary school students, government employees and many others. It is from a prolonged practice of reaching out to the farthest corners of the society around us, that a community of such plural nature has come to be and continues to thrive. FSMK has been at the forefront of advocating for a free society by advocating for software freedom and digital rights. It has been an effective platform for people to voice their concerns against issues like threat to network neutrality,surveillance, privacy, Section 66A, Goonda Act, software patents among others.

Usage of proprietary social networks

It is beyond doubt that a majority of computer users in particular, and citizens in general are networked on proprietary social networks. FSMK is fully aware that the issue is not only about proprietary platforms but also about major corporations capitalizing on the lack of privacy laws and exploiting the users unaware of their digital rights. We have always cried foul of such practices and exposing these issues form a recurring theme in the advocacy of our members.

To reach its longer term goal, it is imperative to engage and be in constant touch with more people and utilize every possible avenue to spread the message of software freedom and related values. Our objective is to engage with those who are locked into such products and services and help them migrate to freer replacements. In this endeavour we have been fairly successful though we acknowledge that we still have a long way to go. Thus, FSMK has utilised every possible medium, including proprietary platforms such as Facebook, Twitter, etc apart from freedom alternatives like Discourse, Mailman, etc to supplement the street campaigns, class-to-class campaigns in colleges and other offline media. FSMK is proud of the free software-hardware contributors who have come into the movement as a result of these exercises.

Having stated the above, FSMK is making all efforts to migrate all organisational platforms to freedom software and solicits help from both within the organisation and outside to help make the transition successful.

How we propose to improve

There are areas to improve and ideally we should be able to move people to more free, decentralized and privacy respecting forms of social networking. While there have been attempts in the past which haven't succeeded the efforts continue.

    Discussion forum on Discourse: Most of the quality discussions that arise on various platforms have already been encouraged to move to FSMK's discussion platform running on ruby-based, Discourse instance accessible at https://discuss.fsmk.org
    Migration to Diaspora: The latest hackathon had an agenda to add features that would make Diaspora a good replacement option. We will continue our effort to contribute code.
    XMPP: Same observation as point 2. We are currently using Telegram for most discussions and these will be moved to compatible XMPP alternatives.

FSMK will fully migrate its official community engagement platforms as soon as we can make it usable for our purpose.
