---
title: ‘Libre Not Gratis’ Merchendise for FSMK Delegates attending FSMI Conference  
category: updates
layout: post
date: 2017-01-07 22:02:00 +0530
discourse_topic_id: 491
---
Bhaiyon aur beheno,
We would like you to direct your attention to this:

Now we ask you to look at it again, in shirt-form:

For dozens of us, this t-shirt meant unity, excitement and a deep commitment to the free philosophy. So much so that FSMK sponsored up to half the price for the initial batch of t-shirts and hoodies. We went on to use a modified version of this design for Camp '16 and even printed a second batch of t-shirts and hoodies due to popular demand. Over time, this design has come to represent a new bond among FSMK volunteers, perhaps even a fresh face for FSMK.

We would now like to take this bond to the next level. We propose that all FSMK delegates wear the Libre Not Gratis design when they attend the second FSMI national conference. This will give us all a great sense of unity and freshness. We reckon we'll also look the coolest, but that's not the point. Most of all, nothing feels better than being part of a crowd proudly wearing a symbol of their community - just ask earlier Libretarians/Gratis-loaders (yes, that's what we call ourselves - aren't we the cutest?).

Please note that this is a voluntary activity and members have to pay for their own t-shirt or hoodie.

The t-shirt costs Rs. 399 and the hoodie costs Rs. 799. They both come in small, medium, large, x-large and xx-large.

We would like to discuss this initiative with all the conference delegates and come to a decision. But we need to decide soon to ensure t-shirts are printed in time. Please reply to this with your comments. We shall also discuss this tomorrow during the general body meeting.

This is a joint venture of FSMK and Away From Keyboard (which is basically Rahul Kondi and Kishan Gupta trying to sound cool). 50% of the proceeds go to FSMK.

Sincerely,
AFK & FSMK
