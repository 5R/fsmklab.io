---
layout: post
title: "FSMK Newsletter Volume 2"
date: 2013-04-08 18:01:27
category: updates
---

Dear all,

We welcome all the members, sympathizers and critics of FSMK to the second edition of our News Letter. This edition discusses different topics that ranges from technical, philosophical, free software politics and activism in depth and breadth.

<a href="http://fsmk.org//sites/default/files/NewsLetter/FSMK Newsletter Volume 2.pdf" style="margin: 0px; padding: 0px; text-decoration: underline; color: rgb(25, 134, 103); font-family: Arial, sans-serif, Helvetica; font-size: 14px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 21px; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);" target="_blank">FSMK News Issue 01</a>


With warm regards,  
FSMK Editorial Board

 
