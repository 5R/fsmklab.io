---
layout: post
title: "Summer Camp 2015 registration opened"
date: 2015-06-05 08:31:40
category: updates
---

Free Software Movement Karnataka (FSMK) is organizing its flagship programme, the Summer Camp 2015, a massive 7-day residential workshop for students of engineering colleges across Karnataka. It's a one-of-its-kind event, and is the fourth one organized by FSMK since 2012.

The focus is on enabling students with skills in Free and Open Source (FOSS) technologies that are being currently used and fast adapted in the industry today. Thus aiming to cater to the industry's growing need for professionals with FOSS skills. Speakers from the industry are invited to share their knowledge and help students work out in the labs to learn a multitude of related technologies during the 7 day period to equip them with applicable skills and valuable experience.

Apart from the technical skills, the camp will also help the students hone their communication, team playing, organizing and leading skills. We also pack in cultural performances, sports and what-nots while we are at it.

**Syllabus – Free Software and Hardware Technologies covered **

 

**Workshop detail :** The entire workshop is split into three tracks featuring Android, System administration and networking and Freedom Hardware(Aurduino) respectively. Android being leading mobile operating system knowing android open up huge opportunities for students for their careers. System administration and networking track is mainly intended for students who is interested in DevOps profile. This is the first time we introduce hardware track in our camp where in hardware design come open and free to modify and distribute. Auruino is kits for building digital devices and interactive objects that can sense and control the physical world.

 

**Net neutrality :** Over the last few months, the issue of net neutrality has been hotly debated by Netizen. Camp will also be having debates, talks and documentary screening connected to the issue of net neutrality. Prominent speakers will be invited who were in forefront fighting for the open internet after the attempt to regulate internet is made recently through savetheinternet campaign 

The event has reached popularity in scores of colleges in Karnataka, to the extent that we get followed up by students about the announcement of the next camp even before we are able to plan for it. Registration for 2015 summer camp already started on 5th June and we are overwhelmed response with registration crossing 100 in first hour itself. For more detail and registration visit http://camp.fsmk.org/
