---
layout: post
title: "Intensifying the campaign for net neutrality"
date: 2015-04-18 14:09:07
category: updates
---

Proposed action items for #netneutrality campaign

1. Even though it is close to the deadline to emtailing TRAI with our replies we think it is essential that the answers percolate down to the local languages.  Can we add an interface for people who are interested to translate the reply documents either in whole or in part which may used in future activities or educational purposes.

2. Once  TRAI and the other committee that was formed comes out with the recommendations it will go to Parliament.  This campaign can be sustained by encouraging people to email the Members of Parliament (MP) of their constituency.  The savetheinternet website may be modified such that email data of the MPs along with the constituencies they represent is provided for people to select and email.

3. Create letters that may be sent to the MPs.  This needs to be done in both English and Hindi at the very least.

4. Create mashup videos of ordinary people giving their views on NN.  
For example, the GLUGs under FSMK have been asked to create at least 5 videos per GLUG which may answer  
          --  What do you understand by net neutrality?  
          --  What do you want the TRAI to recommend to the government?  
          --  What do you want your Member of Parliament to do with respect to net neutrality?  
          --  Give an example of why we need net neutrality

5. Link the issues of NN, Goonda Act, Section 66A in simple language so that the common person understands the significance.  Ensure that regional languages are not missed out.  Show why it is essential that such efforts by the government need to actively resisted.

6. Organise events such as flash mobs, street plays, calling the customer service centres and talking about NN, calling to port away from Airtel/Vodaphone/idea, etc
