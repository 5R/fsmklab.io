---
layout: post
title: "FSMK Camp Fun. Freedom. Knowledge 2014 @DSCE"
date: 2015-03-06 18:35:59
category: updates
---

Another camp from FSMK has completed last weekend (27/July/2014). 9 days, 200 participants and 30 volunteers. Dayananda Sagar college near Banashankari was the venue this time. Software Freedom Law Center (SFLC) has sponsored the event.  In addition to the paid participants there were about 15 students with scholarship also joined us in the camp this time.  3 months long preparation for the camp has culminated in creating some inspired camp participants who echoed the need of GLUG in their campuses and more activities.  They understand the need of freedom in the space of software and technology and net neutrality.

Web-technologies was the theme for this camp. We started the camp by introducing the participants to GNU/Linux commands and shell scripting.  The next days went to more advanced technologies like HTML, Java-scripts, node.js, Mongodb, etc.  There was a mozilla webmaker party and Crypto Party to make the students aware of tools to secure themselves in the surveillance dominated Internet. Mozilla team has helped us on the WebMaker Party.  [Jnaapti][1] has helped us on the Javascript sessions. Gautam from Jnaapti was wondering “How FSMK was not detected in their radar so far”. I was thinking as a reply, “Yes sir.. we are flying too low that others are not able to detect us :) “. By the way flying too low is not a bad thing and requires specialized skills. 

 [1]: http://jnaapti.com/

Students from 30 different engineering colleges across Karnataka have participated in the camp.  
We had to stop the registration early July and restarted later to back fill some of the seats vacated by few after registering for the camp.  Mangalore had the highest registration with more than 40 students from two colleges. There was participation from Kolar, Hassan, Chikkamangalore, Chikka Bellappur, Mysore and Mandya in addition to the 100 registrations from Banglaore.

The organizing team has gone through a tough time in arranging the logistics for the camp, especially labs with enough number of systems.  Most of the participants came up with laptops. But we didn’t have class rooms which can accommodate all participants at the same time.  Tried BigBlueButton for some of the sessions. But it has taken lot of of time for setting up every time for different sessions and locations. Had to shift our class rooms couple of times because of some other commitments by the college. Even with these difficulties, it was one of the best venues we have got for this camp.

Students were grouped to 5 from the beginning so that they could work on a project idea proposed by the volunteers.   The project idea was to build a portal for “Sunday School” of FSMK by using different technologies they are learning through the camp. Every group was assigned one or two volunteers to assist them technically and on logistics.  Every volunteer had other duties in addition to the group mentor-ship.

There were sessions on Free Software Philosophy and mass movement functioning. Documentary based on Aaron Schwartz life, Internet’s own boy, was screened. Some of the participants were literally slipped in to tears after watching this movie.  Sainath’s Nero’s Guest was screened in Hostel.

On Sunday (27th July) Morning around 7:00 AM around 30 participants of the camp divided in to 8 groups went out of the campus and did an exercise around Open Street Maps(OSM). Yogesh from Hassan has guided us on the usage of the mobile application and how to make the contributions to OSM.

There is a new set of people coming forward to take the flags of Free Software Movement Karnataka to newer heights.  Time is changing where organizers need not push for something but there is too much demand for activism, something we dreamed few years back.  Consistent events are spreading the word about the movement to remote places like Chikmangalore and Hassan. This is happening without a full time activist for the organization. As part of the camp we have tried to meet students from each college separately and tried to get their feedback on GLUG functioning if its already present, desire to create a new one if it is not already present. Talked in detail about democratic functioning of these units and need to understand technology to do contributions to the community. Most of the colleges elected their core team which will drive the activities for the next few months and conduct a general body of the respective GLUGs. These core members would elect one or two representatives to the General Body of FSMK also so that they would be part of the state level forum which drives the activities for FSMK through out the state.

Free Software Philosophy and technologies are enablers for many streams of activities which could accommodate people from all walks of life. In addition to the new faces in the volunteers who spend all 9 days in the camp for the seamless execution, there were some old faces who are strongly convinced about the need of this organization.
