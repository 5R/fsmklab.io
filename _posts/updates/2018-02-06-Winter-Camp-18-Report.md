---
layout: post
title: "Completion of Winter Camp 2018; Hassan"
date: 2018-02-06
category: updates
---

# Camp Report

FSMK Winter Camp 2018 was organised at Rajeev Institute of Technology, Hassan from 22nd January to 27th January 2018. Camp consisted of 3 tracks Android, IOT and Ruby. Camp composed of 180 attendees including participants across the 3 tracks, organizers, speakers and volunteers. 

# Infrastructure 

The host college provided with 2 computer labs, seminar hall, Boys hostel with mess, girls hostel, internet facility, breakfast, lunch, dinner. 

# Track Details
This time the Winter Camp had three distinct tracks.
- Android
- IOT
- ROR

# Daily Report 

## Day1 - 22/01/2018:

Day 1 started as everyone assembled in the seminar hall for the inagural programme. The Principal, Vice Principal, CS ,IS and EC hod of RIT were the dignitaries. The respective dignitaries shared their views on the camp followed by the introduction of the speakers, camp leads and track leads. 
Balu D spoke on People's Technology during non tech sessions. After a tea break at 11AM, the technical sessions of the respective tracks commenced. Lunch break was from 1 to 2PM followed by the continuation of respective technical sessions upto 5:30pm with a tea break in between at 4:30pm.
The problems with the bootable pen drives of the participants were sorted out from 6 to 8pm.

## Day2 - 23/01/2018:
    
Day 2 started with breakfast from 8 to 9AM. By 9:30AM, everyone assembled in the seminar hall for the non-tech session during which Biju from SFLC spoke on Data commodification and privacy while voidspacexyz discussed the many problems related to Aadhaar. After a tea break at 11AM, the technical sessions of the respective tracks commenced. Lunch break was from 1 to 2PM followed by a fun activity conducted by Shijil wherein the teams had to invert a human circle without leaving hands. The technical sessions of all 3 tracks continued from 3 to 4:30PM followed by a tea break and the continuation of technical sessions upto 5:30pm. An informal meet and another discussion on Adhaar with voidspacexyz and Ganesh was held from 6:45 to 7:45PM. Later track wise meetings of volunteers were also held.

## Day3 - 24/01/2018:

Day 3 started with breakfast from 8 to 9AM. By 9:00AM the attendees assembled for the non-tech session. voidspacexyz elaborated his talk on adhaar and privacy. 
The technical sessions started at 11:30AM after a tea break until lunch time. Lunch went on from 1:15 to 2:30PM followed by an activity(passing the ball over and under) conducted by Shijil. The technical sessions of the 3 tracks resumed at 3PM and continued till a tea break at 4:45PM up-to 5:45PM. 
Later in the evening, regional meetings were held. The Hassan region meeting was moderated by voidspacexyz while the Mandya, Mangalore and Bangalore meetings were moderated by Shijil.

## Day4 - 25/01/2018:
    
The breakfast was arranged by 8am. By 9:30am all the participants gathered in the seminar hall. "Scientific Temper" was the topic and the speaker was Balaji. The speaker spoke about the importance of basic science in everyone's life and also about the recent innovation. Later around by 11am, tea was arranged for the participants followed by technical sessions.
At 1:00pm lunch was arranged and at 2:30, a small activity (bomb in the city) was organized. Participants actively involved themselves in the activity and by 3pm, people went back to their respective tech sessions. By 4:30pm tea was arranged n by 6pm the tech sessions were concluded. An evening activity called "Ask Us Anything" was arranged which was a open panel discussion and panel members were Balaji, Senthil, Vikram, Shijil and Rama seshan. Later by 8:30pm participants were served with dinner. After dinner people met and discussed about their tracks and things to be done.

## Day5 - 26/01/2018:

The Day started with breakfast at 8:00 AM followed by the Non-Technical session on "Localisation" by Rizma, who spoke about the 'NammaFedora' Project and why localization is an important aspect of FOSS. The session ended by 10:30 AM followed by a Tea - Break post which Tech sessions resumed by 11:00 AM.
The tech sessions were mainly focused on final aspects of the core technology that was promised to be taught in the track.
Sessions broke for lunch at 1:00 PM and resumed at 2:00 PM.
To give participants enough time to freshen up for the cultural night, the sessions ended by 5:00 PM.
The Cultural night began in the auditorium at 6:30 PM, where participants wore ethnic dress and showcased their talent as well, the evening was filled with fun and ended at 9:30.
The dinner was served from 9:30 to 10:30 PM after which track meetings and general interaction session happened.


## Day6 - 27/01/2018:

With this being the last day of the camp, the main focus was on the completion of track syllabus and doubt clearance, the breakfast was served from 8:00 AM - 9:00 AM after which Tech Sessions started.
The tech sessions ended by 1:00 PM after which lunch was arranged and the valedictory session started at 2:30.In the valedictory session the Chairman, Principal, Vice Principal and HOD's of CSE, ISE and ECE of the host college along with the Principal of GECH and Naveen (founding member of FSMK) were thanked by organizing team  for making the camp possible. The certificates were distributed to the participants and volunteers at the end of the session.At 4:00 PM all participants, volunteers and organizers gathered for a group photo. Tea and snacks were served to everyone after the group picture and that marked the end of the day and camp too.


# Looking Forward
This time we have taken steps to make sure that the material used for the camp is not lost after the camp is over. To ensure this we have set up a Gitlab [repository](https://gitlab.com/fsmk/winterCamp2018/) where all the material for all the tracks ahev been stored. The idea is that the material should be available for future reference. 
