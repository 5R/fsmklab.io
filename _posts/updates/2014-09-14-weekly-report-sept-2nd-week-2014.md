---
layout: post
title: "Weekly Report Sept 2nd Week 2014"
date: 2014-09-14 19:54:09
category: updates
---

News

*We The Goondas*.

FSMK has been working with other  
organizations and groups mainly Alternative Law Forum(ALF) and Peoples' Union for Civil Liberties(PUCL) to raise awareness about the amendments to  
the Goondas Act in Karnataka. As of now, the plan is to run a campaign  
around the issue focusing on the inclusion of digital offences under  
the Act and the general arbitrariness of the Act itself. The  
campaign's being called *We The Goondas*.

To highlight the issues with Goonda Act amendments, we plan to host a screening of the biographical documentary about Aaron Swartz who was a not only a victim of an over-reaching judicial system and also a crusader for digital rights and free society.  
We hope to have a meaningful discussion and debate around these issues after watching the movie.  
Get as many people as you can.

<https://www.facebook.com/wethegoondas>

What Happened

*Community centre functioning review meeting held at FSMK Office on 15th Sept

*First meeting held at AC3 community centre On 15th Sept . Classes will be starting from 16th Sep onwards. There are 7 students registered to be part of first batch

Upcoming Activities

*HANDS ON HARDWARE SESSION* at PESSIT on Wednesday on 17th September  
More detail contact Karan Jain (<karanmilan@gmail.com>)
