---
layout: post
title: "Camp Updates"
date: 2014-06-08 19:10:47
category: updates
---

<span style="text-decoration:underline;"><strong>Registrations</strong></span>

Total 223 registration as on now. Still students can apply as those who registered may cancel.

100 + students already paid and got their seats confirmed.For those who has not paid yet , june 19th is last date for making payment. Students who are under waiting list will be informed once the seat is confirmed.

<span style="text-decoration:underline;"><strong>Camp content</strong></span>

Since  main content is finalized already(<http://camp.fsmk.org/technologies>) , currently we are working on preparing hourly based schedule.Last week we met at fsmk office and prepared schedule for day 1. Complete schedule within this week itself.

<span style="text-decoration:underline;"><strong>Sponsorship</strong></span>

Approached couple of IT firms / start-up. As on now no positive response.Going forward we are planning to contact Karnataka Biotechnology and Information Technology Services (KBITS) for sponsorship.We are trying to get meeting fixed with directors

The criteria to accept sponsorship is as follows:

*Any educational institution, govt dept, public sector undertaking can be approached for sponsorship.

*Any organisation, company, individual supporting/advocating the use-propagation of free software and its ideology.

*Any IT company (free or proprietary) as long the product, service they are advertising with us is free software and does not violate the privacy of the service subscribers. Further, they may promote internships, job opportunities for the participating students.

*Sponsors of services like media (radio, television, newspapers-magazines, internet), water, food, venue, accommodation.

*Any well-wisher can contribute in terms of cash or kind.

<http://camp.fsmk.org/sponsors>

Recommend us right people/organizations whom we can approach for sponsorship.

<span style="text-decoration:underline;"><strong>Volunteers</strong></span>

This time we  introduce registration process for volunteers too. All students ( those who has not yet started working in any company) should register the form on or before June 19th.We have already shared  volunteer registration form for all our core student members. Preference will be given to student those who volunteered in any of fsmk camp/workshops. Others can also contribute to camp by volunteering , do let us know your interest.

<span style="text-decoration:underline;"><strong>Scholarship</strong> </span>

This time we allocate max of 20 seats ( subjected to change based on executive committees' decision ) for scholarship. We are working on finalizing terms and conditions. Students from economically poor background and cannot afford making payment  for attending camp are considered for scholarship  Those who apply for scholarship are supposed to submit necessary documents. Will share further detail soon.

Visit our camp website for more detail on camp , <http://camp.fsmk.org>
