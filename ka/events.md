---
layout: dynamic_page
title: ಕಾರ್ಯಕ್ರಮಗಳು
permalink: /ka/events/
language: ka
---

<div class="row row-margin">
  {% for post in site.posts %}
  {% if post.language == 'ka' and post.category =='events' %}
  {% include small_card.html %}
  {% endif %}
  {% endfor %}
</div>
