---
layout: dynamic_page
title: Events
title_kannada: ಕಾರ್ಯಕ್ರಮಗಳು
permalink: /events/
nav: true
---


<div class="row row-margin">
  {% for post in site.posts %}
  {% if post.language != 'ka' and post.category == 'events' %}
  {% include small_card.html %}
  {% endif %}
  {% endfor %}
</div>
