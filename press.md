---
layout: dynamic_page
title: Press
title_kannada: ಪತ್ರಿಕಾ ಹೇಳಿಕೆ
permalink: /press/
nav: true
---


<div class="row row-margin">
  {% for post in site.posts %}
  {% if post.language != 'ka' and post.category == 'press' %}
  {% include small_card.html %}
  {% endif %}
  {% endfor %}
</div>
