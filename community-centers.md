---
layout: page
title: Community Centers
title_kannada: ಸಮುದಾಯ ಕೇಂದ್ರ
permalink: /community-centers/
nav: true
---

Today, information is just a few clicks away. But this knowledge is only accessible to people who are “digitally literate”. Our country is home to over a billion people, out of which about 80% don’t have access to the internet. And those who do are often unaware of all of its possibilities. We are trying to bridge this digital divide by educating people about all the internet can do for them.

One of our initiatives is Community Centers. A Community Center is an initiative in a local community, owned by the community itself – it’s a self sustaining unit. Like all of our initiatives, these centers are volunteer driven. The objective of a Community Center is to help educate the local community – usually children – in the use of computers and the internet. Centers often also provide the members with assistance with academic subjects.

There are multiple Community Centers(Amigo,<a href="/ac3" target="_blank">AC3</a>) in Bangalore. We hope to open more in different parts of Karnataka as well. Please contact us if you are interested in helping us set up or sustain one.
