---
layout: page
title: Ambedkar Community Computing Center (AC3)
title_kannada:
permalink: /ac3/
nav: false
---

The community computing center is an empowerment initiative, headed by the common people, started in the year 2007 to bridge the widening digital divide using Free Software. Computer classes are conducted for everyone, of all walks of life, by the volunteers, who have been the students of AC3 with the support of FSMK volunteers.

In this modern world computers and knowledge of technology alike plays an important role. With the ever growing integration of technology into daily life, lack of that technological knowledge becomes critical for survival. The motto of our AC3 center is “Every common individual needs to have that computer knowledge, to share and follow the philosophy of Free Software”.

We are not only confined to the subject of computers but also conduct other activities such as science, mathematics, spoken language and psychological dance therapy. Our focus is to further discuss and interact about the social issues such as Right to Education (RTE), make sure school dropouts are sent back to school etc., and equip themselves such that they contribute back to society.

Since the center focuses on emotional intelligence, we teach people to dance, enact dramas, and sing to get the best of their personalities. In addition to the activities mentioned above, we also release a monthly magazine, started on April 2017, written and edited using Free Software by the students of AC3.

We have also started a library on AC3 Annual Day Celebration, April 2018  thanks to SFLC, FSMK volunteers, and AID. The library was started with the intention to provide access to knowledge and help people share the same. This is the first step to achieve our goal i.e. to showcase our talent and knowledge to the world.

Currently, the center is run by Vinod Kumar and Renuka with the assistance of other students.
